
public class Temperaturtabelle {

	public static void main(String[] args) {

		//String F0 = "Fahrenheit";
		//String C0 = "Celsius"; //f
		
		double F1 = -20 ;
		double F2 = -10 ;
		double F3 = 0 ;
		double F4 = 20 ;
		double F5 = 30;
		
		
		double C1 = -28.8889 ;
		double C2 = -23.3333 ;
		double C3 = -17.7778 ;
		double C4 = -6.6667 ;
		double C5 = -1.1111 ;
		
		System.out.printf( "\n Fahrenheit  |    Celsius\n");
		System.out.println(" ------------------------");
		System.out.printf( "%-12.0f | %10.2f \n" , F1, C1);
		System.out.printf( "%-12.0f | %10.2f \n" , F2, C2);
		System.out.printf( "+%-12.0f| %10.2f \n" , F3, C3);
		System.out.printf( "+%-12.0f| %10.2f  \n" ,  F4, C4);
		System.out.printf( "+%-12.0f| %10.2f  \n" ,  F5, C5);


	}

}
