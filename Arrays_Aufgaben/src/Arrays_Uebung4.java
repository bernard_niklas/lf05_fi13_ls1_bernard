import java.lang.reflect.Array;

public class Arrays_Uebung4 {

	public static void main(String[] args) {
		// Lottozahlen definiert
		int[] lotto = { 3, 7, 12, 18, 37, 42 };

		System.out.println("Lottozahlen: ");

		// Lotto Ausgabe
		for (int i = 0; i < lotto.length; i++) {
			if (i == 0) {
				System.out.print("[ ");
			}

			System.out.print(lotto[i] + " ");

			if (i == lotto.length - 1) {
				System.out.print("]\n");
			}
		}

		if (istEnthalten(12, lotto) == true) {
			System.out.println("Die Zahl " + 12 + " ist in der Ziehung enthalten.");
		} else {
			System.out.println("Die Zahl " + 12 + " ist nicht in der Ziehung enthalten.");
		}

		if (istEnthalten(13, lotto) == true) {
			System.out.println("Die Zahl " + 13 + " ist in der Ziehung enthalten.");
		} else {
			System.out.println("Die Zahl " + 13 + " ist nicht in der Ziehung enthalten.");
		}

	}

	public static boolean istEnthalten(int x, int[] lotto) {
		boolean status = false;

		for (int i = 0; i < lotto.length; i++) {
			if (lotto[i] == x) {
				status = true;
			}
		}

		return status;
	}
}
