import java.util.*;

public class Arrays_Uebung3 {

	public static void main(String[] args) {
		Scanner txt = new Scanner(System.in);

		System.out.println("Bitte geben Sie 5 Zeichen ein: ");
		String eingabe = txt.next();
		char[] eingabeZeichen = eingabe.toCharArray();

		System.out.println("Umgekehrte Reihenfolge: ");
		for (int i = eingabeZeichen.length; i <= eingabeZeichen.length && i != 0; i--) {
			System.out.print(eingabeZeichen[i - 1] + "");
		}

	}

}
