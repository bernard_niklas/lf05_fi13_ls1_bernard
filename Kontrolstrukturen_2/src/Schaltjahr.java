import java.util.Scanner;

public class Schaltjahr {

	public static void main(String[] args) {
		// Deklaration
		Scanner tastatur = new Scanner(System.in);
		int jahresEingabe;
		
		// Zuweisung
		System.out.println("Bitte geben sie das zu Überprüfende Jahr ein ");
		jahresEingabe = tastatur.nextInt();
		
		if(jahresEingabe % 4 == 0) {
			if(jahresEingabe % 100 != 0 || jahresEingabe % 400 == 0) {
				System.out.println("Das Jahr '" + jahresEingabe + "' ist ein Schaltjahr.");
			} else {
				System.out.println("Dieses Jahr ist kein Schaltjahr!");
			}
		} else {
			System.out.println("Dieses Jahr ist kein Schaltjahr!");
		}

	}

}
