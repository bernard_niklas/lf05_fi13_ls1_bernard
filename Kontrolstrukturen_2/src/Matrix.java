import java.util.Scanner;

public class Matrix {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		int inputZahl, m = 0;
		System.out.println("Wählen sie bitte eine Zahl zwischen 2 und 9 aus");
		inputZahl = tastatur.nextInt();
		System.out.println("Die Matrix wird generiert");
		   for (int i = 0; i < 28; i++)
		    {
		       System.out.print("=");
		       try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		    }
		    System.out.println("\n\n"); 
		
		while(m < 100) {
			
			if(m % 10 == 0 && m != 0) {
				System.out.println("");
			}
			
			if(m%inputZahl == 0 && m!=0) {
			System.out.print(" * ");
			} else {
			System.out.print(m + " ");
			}
			m++;
		}
		
	}

}
