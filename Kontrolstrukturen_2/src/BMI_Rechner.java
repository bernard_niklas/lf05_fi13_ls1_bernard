import java.util.Scanner;

public class BMI_Rechner {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie ihr Gewicht in Kilogramm kg an");
		
		double gewicht = tastatur.nextDouble();
		
		System.out.println("Bitte geben Sie ihre Körpergröße in Centimeter cm an");
		
		double groesse = tastatur.nextDouble();
		double kgQuadrat = (groesse/100) * (groesse/100);
		
		double bmi = gewicht/kgQuadrat;
		System.out.printf("%.2f" ,bmi);
		System.out.println();
		System.out.println("Bitte wählen Sie ihr Geschlecht (m/w)");
		String geschlecht = tastatur.next();
		
		if (geschlecht.equals("m")) {
			if (bmi<20) {
				System.out.println("Sie sind untergewichtig");
			}
				else if (bmi>=20 && bmi>=25) {
					System.out.println("Sie sind normalgewichtig");
			}
				else if (bmi>20) {
					System.out.println("Sie sind übergewichtig");
				}
		
		}
		
		else if (geschlecht.equals("w")) {
			if (bmi<19) {
				System.out.println("Sie sind untergewichtig");
				
			}
			else if (bmi>=19 && bmi>=24) {
				System.out.println("Sie sind normalgewichtig");
		}
			else if (bmi>24) {
				System.out.println("Sie sind übergewichtig");
			}
			
		}
		
		else {
			System.out.println("Eingabefehler");
		}

	}

}
