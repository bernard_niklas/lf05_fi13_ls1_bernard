import java.util.Scanner;

public class Fahrkartenautomat_Methoden1 {

	public static void main(String[] args) {
		 double antwort = 0;
	do {
		Scanner tastatur = new Scanner(System.in);
		
		/*
		Arrays sorgen für einen sauberen und übersichtlicheren quellcode es
		bedarf weniger anpassungen der Nachteil von Arrays ist jedoch ihr komplexer Aufbau und 
		der damit höhere Aufwand um Fehler auszumachen
		*/

		 double zuZahlenderBetrag; 
		 double rückgabebetrag;
		 
		zuZahlenderBetrag = fahrkartenbestellungenErfassen(tastatur);
		rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);
		 					fahrkartenAusgeben();
		 					rückgeldAusgeben(rückgabebetrag);
		 
		    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                    "vor Fahrtantritt entwerten zu lassen!\n"+
                    "Wir wünschen Ihnen eine gute Fahrt.");
		    
		    System.out.println("Möchten sie eine weitere Fahrkarte kaufen?");
		    System.out.println();
		    System.out.println("1) Ja ich möchte eine weitere Fahrkarte kaufen");
		    System.out.println("2) Nein ich benötige keine weitere Fahrkarte");
		    antwort= tastatur.nextDouble();
		    
	}while(antwort==1);
		    
		    	
		    
		    
		   
	}
	
public static double fahrkartenbestellungenErfassen(Scanner tastatur) {                                //Vor- und Nachteile von Arrays ganz unten
        
        System.out.println("Wie viele Fahrkarten wollen Sie?");
        int count = tastatur.nextInt();
        String ticketnr = "Ticketnummer";
        String bzn = "Bezeichnung";
        String preis = "Preis in Euro";
                    

        String [] bezeichnungen = new String [10];
        bezeichnungen[0] = "Einzelfahrschein Berlin AB";
        bezeichnungen[1] = "Einzelfahrschein Berlin BC";
        bezeichnungen[2] = "Einzelfahrschein Berlin ABC";
        bezeichnungen[3] = "Kurzstrecke";
        bezeichnungen[4] = "Tageskarte Berlin AB";
        bezeichnungen[5] = "Tageskarte Berlin BC";
        bezeichnungen[6] = "Tageskarte Berlin ABC";
        bezeichnungen[7] = "Kleingruppen-Tageskarte Berlin AB";
        bezeichnungen[8] = "Kleingruppen-Tageskarte Berlin BC";
        bezeichnungen[9] = "Kleingruppen-Tageskarte Berlin ABC";
        
        double[] preise = new double[10];
        preise[0] = 2.90;
        preise[1] = 3.30;
        preise[2] = 3.60;
        preise[3] = 1.90;
        preise[4] = 8.60;
        preise[5] = 9.00;
        preise[6] = 9.60;
        preise[7] = 23.50;
        preise[8] = 24.30;
        preise[9] = 24.90;
          
        System.out.println("Hier die �bersicht der Tickets:\n");
        System.out.printf("%-20s%-40s%-20s\n", ticketnr, bzn, preis);
        System.out.println("==================================================================================");
        for(int i = 0; i < bezeichnungen.length; i++) {
            System.out.printf("%-20s%-40s%.2f\n", i+1, bezeichnungen[i],preise[i]);
        }
        
        System.out.println("\nBitte geben Sie die gew�nschte Ticketnummer ein.");
        int Ticketnummer = tastatur.nextInt();
        double zuZahlenderBetrag = preise[Ticketnummer-1]*count;
        System.out.printf("Zu zahlender Betrag: %.2f Euro \n", zuZahlenderBetrag);
        
        return zuZahlenderBetrag;
    }
 
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag, Scanner tastatur) {
		double eingeworfeneMünze;
		double eingezahlterGesamtbetrag;
		
		eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("Noch zu zahlen:" + "%.2f EURO \n",(zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   //println wurde von mir durch printf ersetzt und die Formattierung eingefügt - N.Bernard
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
		
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;  //Wird eventuell in die Rückgabe Methode verschoben
		
		return rückgabebetrag;
	}
	
	
	public static void fahrkartenAusgeben() {
	
	System.out.println("\nFahrschein wird ausgegeben");
    for (int i = 0; i < 28; i++)
    {
       System.out.print("=");
       try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
    System.out.println("\n\n"); 
    
} 
	
	public static void rückgeldAusgeben(double rückgabebetrag) {
		
		if(rückgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f EURO \n" ,rückgabebetrag);
	    	   System.out.println("\nwird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2,00 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1,00 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }
		
	}
	
	
    
	
	
    
}

